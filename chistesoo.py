#!/usr/bin/python3
# -*- coding: utf-8 -*-

import xml.dom.minidom


def main(path):
    obj = Humor(path)
    for joke in obj.jokes():
        print(f"Calificación: {joke['score']}.")
        print(f" Respuesta: {(joke['answer'])}")
        print(f" Pregunta: {(joke['question'])}\n")


class Humor:
    def __init__(self, path):
        self.path = path

    def jokes(self):
        calificaciones = ['buenisimo', 'bueno', 'regular', 'malo', 'malisimo']
        document = xml.dom.minidom.parse(self.path)
        jokes = document.getElementsByTagName('chiste')
        chistes = []
        resultado = []

        for joke in jokes:
            score = joke.getAttribute('calificacion')
            questions = joke.getElementsByTagName('pregunta')
            question = questions[0].firstChild.nodeValue.strip()
            answers = joke.getElementsByTagName('respuesta')
            answer = answers[0].firstChild.nodeValue.strip()
            chistes.append((score, question, answer))

        for calif in calificaciones:
            for chiste in chistes:
                chiste_calif = chiste[0]
                if chiste_calif == calif:
                    resultado.append({'score': chiste_calif,
                                      'answer': chiste[2],
                                      'question': chiste[1]})

        return resultado


if __name__ == "__main__":
    main('tests/chistes8.xml')
