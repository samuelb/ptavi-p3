#!/usr/bin/python3
# -*- coding: utf-8 -*-

import sys
import smil


def main(possible_filename):
    try:
        filename = str(possible_filename)
    except:
        print('Usage: python3 elements.py <file>')
    else:
        file = smil.SMIL(filename)
        for element in file.elements():
            print(element.name())


if __name__ == '__main__':
    main(sys.argv[1])
