#!/usr/bin/python3
# -*- coding: utf-8 -*-

import json
import smil


class SMILJSON(smil.SMIL):

    def json(self):
        a = []
        smil_file = smil.SMIL(self.path)

        for element in smil_file.elements():
            element_obj = ElementJSON(element.name(), element.attrs())
            a.append(element_obj.dict())

        json_string = json.dumps(a, indent=1)
        return json_string


class ElementJSON(smil.Element):

    def dict(self):
        json_dict = {'attrs': self.attr_dict, 'name': self.name()}
        return json_dict


def main(path):
    x = SMILJSON(path)
    x.json()
    print(x.json())


if __name__ == '__main__':
    main('karaoke.smil')
