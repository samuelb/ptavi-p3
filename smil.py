#!/usr/bin/python3
# -*- coding: utf-8 -*-

import xml.dom.minidom
import xml.etree.ElementTree


class Element:
    def __init__(self, name, attrs):
        self.element_name = name
        self.attr_dict = attrs

    def name(self):
        return self.element_name

    def attrs(self):
        return self.attr_dict


class SMIL:
    def __init__(self, path):
        self.path = path

    def elements(self):
        document = xml.etree.ElementTree.parse(self.path)
        elements = list()
        elementTypeList = list()

        for elem in document.iter():
            elements.append(elem)

        for element in elements:
            elementTypeList.append(Element(element.tag, element.attrib))

        return elementTypeList


if __name__ == '__main__':
    obj = SMIL('karaoke.smil')
    x = obj.elements()
