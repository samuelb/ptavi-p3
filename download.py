#!/usr/bin/python3
# -*- coding: utf-8 -*-

from urllib.request import urlretrieve
import sys

import smil


def main(path):

    if len(sys.argv) < 2:
        sys.exit('Usage: python3 elements.py <file>')

    else:
        smil_elements = smil.SMIL(path).elements()
        src_attrs = list()
        for elem in smil_elements:
            attrs = elem.attrs()
            attrs_keys = attrs.keys()
            for attr in attrs_keys:
                if attr == 'src':
                    src_attrs.append(attrs['src'])

        count = 0
        for url in src_attrs:
            if url.startswith('http://') or url.startswith('https://'):
                ext = url.split('.')[-1]
                filename = f'fichero-{count}.{ext}'
                urlretrieve(url, filename)
                count += 1


if __name__ == '__main__':
    main(sys.argv)
