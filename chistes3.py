#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""Simple program to parse a chistes XML file"""

import xml.dom.minidom


def main(path):
    calificaciones = ['buenisimo', 'bueno', 'regular', 'malo', 'malisimo']
    document = xml.dom.minidom.parse(path)
    jokes = document.getElementsByTagName('chiste')
    chistes = []

    for joke in jokes:
        score = joke.getAttribute('calificacion')
        questions = joke.getElementsByTagName('pregunta')
        question = questions[0].firstChild.nodeValue.strip()
        answers = joke.getElementsByTagName('respuesta')
        answer = answers[0].firstChild.nodeValue.strip()
        chistes.append((score, question, answer))

    for calif in calificaciones:
        for chiste in chistes:
            chiste_calif = chiste[0]
            if chiste_calif == calif:
                print(f"Calificación: {calif}.")
                print(f" Respuesta: {(chiste[2])}")
                print(f" Pregunta: {(chiste[1])}\n")


if __name__ == "__main__":
    main('tests/chistes8.xml')